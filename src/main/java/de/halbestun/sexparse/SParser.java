/*
 * Loosely based upon http://norvig.com/lispy.html
 */
package de.halbestun.sexparse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Till Hartmann
 */
public class SParser
{

	public static Expression parse(String message)
	{
		return parse(tokenize(message));
	}

	private static Queue<String> tokenize(String s)
	{
		String[] ts = s.trim().replaceAll("\\(", "( ").replaceAll("\\)", " )").replaceAll("\\)\\(", ") (").trim().split(" ");
		return new LinkedList<>(Arrays.asList(ts));
	}

	private static Expression parse(Queue<String> tokens)
	{
		if (tokens.isEmpty())
		{
			throw new RuntimeException("empty message");
		}
		String token = tokens.poll();
		switch (token)
		{
			case "(":
				final ExpressionList list = new ExpressionList();
				while (!tokens.peek().equals(")"))
				{
					list.add(parse(tokens));
				}
				tokens.poll();
				return list;
			case ")":
				throw new RuntimeException("Unexpected )");
			default:
				return new Atom(token);
		}
	}

	public class ParseException extends RuntimeException
	{

	}
}
