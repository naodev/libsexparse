/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.halbestun.sexparse;

/**
 *
 * @author till
 */
public class Atom implements Expression
{
    private final String text;
    
    public Atom(String text)
    {
        this.text = text;
    }
    
    @Override
    public String toString()
    {
        return this.text;
    }
}
