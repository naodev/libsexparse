/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.halbestun.sexparse;

import java.util.Arrays;
import java.util.Iterator;
import java.util.ArrayList;

public class ExpressionList extends ArrayList<Expression> implements Expression
{

    ExpressionList parent = null;
    int indent = 1;

    public int getIndent()
    {
        if (parent != null)
        {
            return parent.getIndent() + indent;
        }
        else
        {
            return 0;
        }
    }

    public void setIndent(int indent)
    {
        this.indent = indent;
    }

    public void setParent(ExpressionList parent)
    {
        this.parent = parent;
    }

    @Override
    public String toString()
    {
        String indented = "";
        if (parent != null && parent.get(0) != this)
        {
            indented = "\n";
            char[] chars = new char[getIndent()];
            Arrays.fill(chars, ' ');
            indented += new String(chars);
        }

        String output = indented + "(";
        for (Iterator<Expression> it = this.iterator(); it.hasNext();)
        {
            Expression expr = it.next();
            output += expr.toString();
            if (it.hasNext())
            {
                output += " ";
            }
        }
        output += ")";
        return output;
    }

    @Override
    public synchronized boolean add(Expression e)
    {
        if (e instanceof ExpressionList)
        {
            ((ExpressionList) e).setParent(this);
            if (size() != 0 && get(0) instanceof Atom)
            {
                ((ExpressionList) e).setIndent(2);
            }
        }
        return super.add(e);
    }

}
